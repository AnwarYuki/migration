<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('tugas.register');
    }

    public function welcome(Request $request){

        $fname = $request['nama_depan'];
        $lname = $request['nama_belakang'];

        return view('tugas.welcome', compact('fname', 'lname'));
    }
}

